<?php

namespace App\Mail;

use DB;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Reminder extends Mailable
{
    use Queueable, SerializesModels;
    public $registration;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($code)
    {
        $this->registration = DB::table('registrations')->where('unique_code', $code)->first();
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from(['NARS Malaysia' => 'nars-lip-vinyl-dev@webqlo.com'])
            ->subject('Reminder to NARS Lip Vinyl Event!')
            ->view('email.reminder');
    }
}
