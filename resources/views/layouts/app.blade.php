<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-113623625-5"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-113623625-5');
    </script>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <meta name="description" content="NARS. RADIANCE REPOWERED"/>

    <meta property="og:locale" content="en" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="NARS Malaysia - NARS Radiance" />
    <meta property="og:description" content="NARS. RADIANCE REPOWERED" />
    <meta property="og:url" content="http://nars-radiance.developer.webqlo.com/" />
    {{--<meta property="og:image" content="http://nars-holiday-2018.developer.webqlo.com/images/nars_holiday_bg_mobile.png" />--}}
    <meta property="og:site_name" content="NARS Malaysia" />
    <link href="../images/favicon.ico" rel="shortcut icon" />
    <!-- Styles -->
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/pretty-checkbox.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    @stack('css')
</head>
<body>
    <div id="app">
        @yield('content')
    </div>

    <!-- Modals -->
    @stack('modal')

    <!-- Scripts -->
    <script src="{{ asset('js/jquery-3.3.1.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('js/system.js') }}"></script>
    @stack('js')
</body>
</html>
