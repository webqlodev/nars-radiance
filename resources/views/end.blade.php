@extends('home')

@section('form')
<div class="end-text">
    <p class="lead">
        The campaign has ended on <br>{{ Carbon\Carbon::parse( $end )->format('j F Y, ga') }}
    </p>

    <p>
        Thank you for your participation. <br>Please stay tuned to our upcoming events.
    </p>
</div>
@endsection
