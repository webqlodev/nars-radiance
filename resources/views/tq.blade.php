@extends('layouts.app')

@section('content')

<div class="container-fluid top">
    <div class="background-image">
    </div>
    
    <div class="row">
        <div class="col-lg-6 tq-wrapper">
            <img src="/images/nars_logo.png" alt="" class="tq-logo-img">
            <div class="tq-padding">
                <h1 class="tq-title">THANK YOU, NARSISSIST.</h1>
                <h3 class="tq-subtitle">We have sent you an email with your redemption code. Check your spam <span>folder if you cannot find it in your inbox.</span></h3>

                <h3 class="tq-slogan">NARS Cosmetics is available at Pavilion KL, Suria KLCC, Mid Valley Megamall, Sunway Pyramid, AEON Tebrau City, SkyAvenue Genting and Parkson Gurney.</h3>
            </div>
        </div>
    </div>
    <!-- <div class="tnc text-center">
        
        <p>*Limited quantities available. Items may vary at different stores. NARS reserves the right to modify or amend the validity of this offer without prior notice.<span>Other terms and conditions apply. NARS Cosmetics is available at Pavilion KL, Suria KLCC, Mid Valley Megamall, Sunway Pyramid, AEON Tebrau City and Parkson Gurney.</span></p>
        
    </div> -->
</div>


@endsection

@push('js')
<script>
    $(function () {
        $('#video').on('click', function() {
            if($(this)[0].paused) {
                $(this)[0].play();
            }
            else {
                $(this)[0].pause();
            }
        });
    });
</script>
@endpush
