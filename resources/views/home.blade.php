@extends('layouts.app')

@section('content')
<div class='header-section'>
    <div class='custom-container p-0'>
        <div class="header-logo">
            <a href="#" class="nav-link"><img src="../images/nars_logo.png" class='img-fluid header-logo-img'/></a>
        </div>
    </div>
</div>

<div class='body-section'>
    <div class='asian-radiance-empowered-section custom-container p-0'>
        <div class='row m-0'>
            <div class='col-12 p-0'>
                <div class='empowered-img'>
                    <div class='display-desktop d-none d-md-block'>
                        <img src="../images/desktop_empowered.jpg" class='img-fluid w-100'/>
                    </div>
                    <div class='display-desktop d-block d-md-none'>
                        <img src="../images/mobile_empowered.jpg" class='img-fluid w-100'/>
                    </div>
                </div>
                <div class='empowered-desc display-desktop d-none d-md-block'>
                    <h1 class='empowered-title m-0'>
                        radiance.<br/>
                        repowered.
                    </h1>   
                    <p class='empowered-desc-content'>
                        Discover a radiance-boosting lineup 
                        <br class="d-none d-md-block">to reveal skin at its absolute best.
                    </p>
                </div>
                <div class='empowered-mobile-desc display-mobile d-block d-md-none'>
                    <h1 class='empowered-title m-0'>
                        RADIANCE.<br>REPOWERED.
                    </h1>
                    <p class='m-0 empowered-desc-content'>
                        Discover a radiance-boosting lineup
                        <br class="d-none d-md-block">to reveal skin at its absolute best.
                    </p>
                </div>
            </div>
        </div>
    </div>
    <div class='supernatural-section custom-container p-0 '>
        <div class='row m-0'>
            <div class='col-12 p-0'>
                <div class='supernatural-img'>
                    <div class='display-desktop d-none d-md-block'>
                        <img src="../images/desktop_supernaturals.jpg" class='img-fluid w-100'/>
                    </div>
                    <div class='display-desktop d-block d-md-none'>
                        <img src="../images/mobile_supernatural.jpg" class='img-fluid w-100'/>
                    </div>
                </div>
                <div class='supernatural-desc display-desktop d-none d-md-block'>
                        <h2 class='supernatural-title m-0'>
                            <div class="LogoText">
                                NARS 
                            </div> 
                            supernaturals
                        </h2>
                        <p class="supernatural-desc-content">
                            Intensify your glow with new  <br/>Super Radiant Booster and
                            <br/> NARS' radiant essentials.
                        </p>
                </div>
                <div class='supernatural-mobile-desc display-mobile d-block d-md-none'>
                <h2 class='supernatural-title m-0'>
                    <div class="LogoText">
                        NARS
                    </div> 
                    supernaturals
                </h2>
                <p class="supernatural-desc-content m-0">
                    Intensify your glow with <br/>
                            new Super Radiant Booster and
                            <br/> NARS' radiant essentials.
                </p>
                </div>
             </div>
        </div>
    </div>
     <div class='video-section custom-container p-0'>
        <div class="embed-responsive embed-responsive-16by9">
          <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/CPweoAOFQXI?playsinline=1" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
      </div>
    </div>
    <div class='sp19-radiance-empowered-section custom-container p-0 '>
        <div class='row m-0'>
            <div class='col-12 p-0'>
                <div class='supernatural-img'>
                    <div class='display-desktop d-none d-md-block'>
                        <img src="../images/nars_sp19_radiancerepowered_homepage_desktop_asia.jpg" class='img-fluid w-100'/>
                    </div>
                    <div class='display-desktop d-block d-md-none'>
                        <img src="../images/nars_sp19_radiancerepowered_homepage_mobile_asia.jpg" class='img-fluid w-100'/>
                    </div>
                </div>
             </div>
        </div>
    </div>
    <div class='sp19-radiance-empowered-text-section custom-container p-0 bg-red'>
        <div class='row m-0'>
            <div class='col-12 p-0'>
                <div class='sp19-desc display-desktop d-none d-md-block'>
                        <h2 class='sp19-title m-0'>
                            Visit NARS Pavilion Consumer Event at main entrance <br>from 18 &ndash; 24 March 2019 to receive <br>a 3-pc gift set with RM320 worth of purchase, inclusive at least 1 <br>NARS complexion product.
                        </h2>
                        <p class="sp19-desc-content">
                            *Exclusively for NARS Pavilion Consumer Event from 18 &ndash; 24 March 2019. Limited quantities available. NARS reserves the <br>right to modify or amend the validity. Terms and conditions apply.
                        </p>
                </div>
                <div class='sp19-mobile-desc display-mobile d-block d-md-none'>
                <h2 class='sp19-title m-0'>
                    Visit NARS Pavilion Consumer Event at main entrance from 18 &ndash; 24 March 2019 to receive a 3-pc gift set with RM320 worth of purchase, inclusive at least 1 NARS complexion product.
                </h2>
                <p class="sp19-desc-content m-0">
                    *Exclusively for NARS Pavilion Consumer Event from 18 &ndash; 24 March 2019. Limited quantities available. NARS reserves the right to modify or amend the validity. Terms and conditions apply.
                </p>
                </div>
             </div>
        </div>
    </div>
    <div class='notice-section custom-container p-0 d-none'>
         <div class='row m-0'>
            <div class='col-12 promo-text-container'>
                <p class='promo-text m-0'>
                    *Complimentary three-piece gift eligible with Super Radiant Booster purchase only. Limited quantities available. Items may vary at different stores. NARS reserves the right to modify or amend the validity  of this offer without prior notice. Other terms and conditions apply. <span class="d-inline-block">&nbsp;NARS Cosmetics is available at Pavilion KL, </span><span class='d-inline-block'>Suria KLCC, Mid Valley Megamall, </span><span class='d-inline-block'> Sunway Pyramid, Parkson Gurney, SkyAvenue Genting and AEON Tebrau City.</span>
                </p>
            </div>
        </div>
    </div>
    <div class='mobile-notice-section custom-container p-0 d-none'>
         <div class='row m-0'>
            <div class='col-12 promo-text-container'>
                <p class='mobile-rules text-center'>
                    <span class='d-inline-block'>*Complimentary three-piece gift eligible with Super Radiant Booster </span>
                    <span class='d-inline-block'>purchase only. Limited quantities available. Items may be vary at  </span>
                    <span class='d-inline-block'>different stores.NARS reserves the right to modify or amend the </span><span class='d-inline-block'>validity of this offer without prior notice. Other terms and conditions </span><span class='d-inline-block'> apply. NARS Cosmetics is available at Pavilion KL, Suria KLCC, </span><span class='d-inline-block'>Mid Valley Megamall, Sunway Pyramid,  Parkson Gurney,</span><span class='d-inline-block'>SkyAvenue Genting and AEON Tebrau City.</span>    
                </p>
            </div>
        </div>
    </div>
    <div class='submit-section custom-container'>
        <div class='submit-content'>
            @if (Carbon\Carbon::parse( env('END_TIME') ) > Carbon\Carbon::now())
            <div class="row m-0 form-section">
                <div class='col-12 text-center p-0'>
                    <h2 class='submit-title'>
                        redeem your sample now
                    </h2>
                    <form action="{{ route('submit') }}" id='entryForm' enctype="multipart/form-data">
                        <div class='row m-0 form-content-center form-row'>
                            <div class='col-12 col-sm-7 email-section'>
                                <div class="form-group" id='emailFormGroup'>
                                    <input id="email" type="email" name="email" placeholder="YOUR@EMAIL.COM" class="field-large emailfield newsletter-field-inline">
                                    <span class="help-block  alert-danger d-none" id='emailError'></span>
                                </div>
                            </div>
                            {{ csrf_field() }}
                            <div class='col-12 col-sm-3 btn-redeem-section p-0'>
                                <button type='submit' class='btn btn-dark btn-redeem'>Submit</button> 
                            </div>
                        </div>
                    </form>
                    <div class='tnc-section'>
                        <div class='row m-0 tnc-row'>
                            <div class="form-group" id='termsConditionFormGroup'>
                                <div class='row m-0 form-content-center form-row'>
                                    <div class="col-1 m-0 checkbox-section p-0 float-left">
                                        <input type="checkbox" name="termsCondition" id='termsCondition'>
                                    </div>
                                    <div class='tnc-content col-10'>
                                        I agree that the collection and processing of my personal data will be in compliance with the NARS Cosmetics <a href="#" data-toggle="modal" data-target="#privacy_modal">Privacy Policy</a>, and I agree that my use of this website will be in compliance with these <a href="#" data-toggle="modal" data-target="#tnc_modal">Terms &amp; Conditions</a>.
                                        <span class="help-block  alert-danger d-none terms-error" id='termsConditionError'></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> 
                <div class='rules-desktop custom-container rules-section d-block d-md-none'>
                    <div class='rules-container'>
                        <div class='row'>
                            <div class='col-12 mobile-view-rules p-0'>
                                <p class='mobile-rules text-center'>
                                    <span class='d-inline-block'>*Limited quantities available. Items may be vary at different stores. </span>
                                    <span class='d-inline-block'>NARS reserves the right to modify or amend the validity of this </span>
                                    <span class='d-inline-block'>offer without prior notice. Other terms and conditions apply. </span>
                                    <span class='d-inline-block'>NARS Cosmetics is available at Pavilion KL, Suria KLCC, </span>
                                    <span class='d-inline-block'>Mid Valley Megamall, Sunway Pyramid, Parkson Gurney, </span>
                                    <span class='d-inline-block'>SkyAvenue Genting and AEON Tebrau City.</span>    
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @else
            <div class='row m-0 text-center'>
                <p class="lead col-12">
                    The campaign has ended on <br>{{ Carbon\Carbon::parse( env('END_TIME') )->format('j F Y, ga') }}
                    
                </p>

                <p class="lead col-12">
                    Thank you for your participation. <br>Please stay tuned to our upcoming events.
                </p>
                <div class='col-12 mobile-view-rules p-0 d-none d-lg-block'>
                    <p class='mobile-rules text-center'>
                        <span class='d-inline-block'>*Limited quantities available. Items may be vary at different stores. </span>
                        <span class='d-inline-block'>NARS reserves the right to modify or amend the validity of this </span><span class='d-inline-block'>offer without prior notice. Other terms and conditions apply. </span><span class='d-inline-block'>NARS Cosmetics is available at Pavilion KL, Suria KLCC, </span><span class='d-inline-block'>Mid Valley Megamall, Sunway Pyramid, Parkson Gurney, </span><span class='d-inline-block'>SkyAvenue Genting and AEON Tebrau City.</span>    
                    </p>
                </div>
            </div>
            @endif
        </div>
    </div>
    <div class='rules-desktop custom-container rules-section d-none d-md-block'>
        <div class='rules-container'>
            <div class='row'>
                <div class='col-12 desktop-view-rules'>
                    <p class='desktop-rules text-center'>
                        <span class='d-inline-block'>*Limited quantities available. Items may be vary at different stores. </span>
                        <span class='d-inline-block'>NARS reserves the right to modify or amend the </span><span class='d-inline-block'>validity of this offer without prior notice. Other terms and conditions apply.</span><span class="d-none d-xxl-block"></span><span class="d-inline-block">&nbsp;NARS Cosmetics is available at Pavilion KL, </span><span class='d-inline-block'>Suria KLCC, Mid Valley Megamall, </span><span class='d-inline-block'> Sunway Pyramid, Parkson Gurney, SkyAvenue Genting and AEON Tebrau City.</span>    
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>

<div class='footer-section'>
</div>

<div class="modal" id="thankyouModal">
    <div class="modal-dialog modal-xl modal-dialog-centered">
        <div class="modal-content">

            <!-- Modal body -->
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal">
                    &times;
                </button>
                <!-- <img src='../images/thank-you-bg.jpg' class='img-fluid'/> -->
                <div class='thank-you-content'>
                    <h2 class='text-right thank-you-title'>
                        Thank You
                    </h2>
                    <p class='text-right thank-you-content-desc'>
                        We have sent you an email <br/> with your redemption code. <br/> Check your spam folder if  <br/> you cannot find it in your inbox.
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="errorModal">
    <div class="modal-dialog modal-xl modal-dialog-centered">
        <div class="modal-content">

            <!-- Modal body -->
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal">
                    &times;
                </button>
                <!-- <img src='../images/thank-you-bg.jpg' class='img-fluid'/> -->
                <div class='thank-you-content'>
                    <h2 class='text-right thank-you-title'>
                        Oopss...
                    </h2>
                    <p class='text-right thank-you-content-desc'>
                        Something wrong with the server. Please try again later. 
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="privacy_modal" class="modal" tabindex="-1" role="dialog" aria-labelledby="privacy_modal_title">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="privacy_modal_title">Privacy Policy</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                
            </div>
            <div class="modal-body">
                <p>
                    If there is any conflict between the Bahasa Malaysia and English versions of this Privacy Policy, the English version shall prevail.
                </p>

                <p>
                    Shiseido Malaysia Sdn. Bhd. (“Shiseido” or “We”) is committed to protecting your privacy and ensuring that your Personal Data is protected. For the purposes of this Privacy Policy, "Personal Data" means any personally identifiable data, whether true or not, about an individual who can be identified from that data.
                </p>

                <ol class='pnc-ol'>
                    <li>
                        APPLICATION

                        <ul>
                            <li>
                                This Privacy Policy explains the types of Personal Data we collect and how we use, disclose, transfer, process and protect that information. 
                            </li>
                            <li>
                                We collect Personal Data through, but not limited to, the following means:
                                <ol>
                                    <li type="a">
                                        When you shop on or browse <a href='https://www.shiseido.com.my' target='_blank'>[https://www.shiseido.com.my]</a> (the “Website”);
                                    </li>
                                    <li type="a">
                                       When you shop in-store at our physical stores;
                                    </li>
                                    <li type="a">
                                       When you connect with us through social media or attend our marketing events; and
                                    </li>
                                    <li type="a">
                                       When you agree and consent to be a member of the Shiseido Membership, whether through physical or electronic means. 
                                    </li>
                                </ol>
                            </li>
                            <li>
                                We may update this Privacy Policy from time to time by posting updated versions on the Website, and/or by sending an e-mail to you. Your continued membership, access to and/or use of the Website will be taken to be your agreement to, and acceptance of, all changes made in each updated version. 
                            </li>
                            <li>
                                Please check back regularly for updated information on how we handle your Personal Data.
                            </li>
                        </ul>
                    </li>

                    <li>
                       CONSENT
                        <ul>
                            <li>
                                We do not collect, use or disclose your Personal Data without your consent (except where permitted and authorised by law). By providing your Personal Data to us, you hereby consent to us collecting, using, disclosing, transferring, and processing your Personal Data for the purposes set out in Section 3 of this Privacy Policy.
                            </li>

                            <li>
                                The types of Personal Data we collect include, but are not limited to, your: (a) first name and family name; (b) home address; (c) date of birth; (d) email address; and, only if appropriate, your (e) user name and password; (f) billing and delivery address; (g) personal identification number; and (h) other information as may be reasonably required for us to provide you with the Services as defined in Section 3 below. 
                            </li>

                            <li>
                                Communicating with you about our products and services, in particular about promotional offers, marketing information and event programs;
                            </li>

                        </ul>
                    </li>

                    <li>
                       PURPOSE
                        <ul>
                            <li>
                                We collect, use, disclose, transfer and process your Personal Data for the purpose of providing services. These services include, but are not limited to:
                                <ol>
                                    <li>providing you with information on products and campaigns from us, Shiseido Group and our third party business partners via email, SMS, and post (where we have your express consent);</li>
                                    <li>allowing you to purchase products and services offered for sale via the Website;</li>
                                    <li>facilitating your transactions with us;</li>
                                    <li>sending you product samples and/or products;</li>
                                    <li>keeping you informed of updates, changes, and developments relating to us and our Services;</li>
                                    <li>notifying you about important changes to this Privacy Policy, and to our other policies or services;</li>
                                    <li>providing you with personalized consultations;</li>
                                    <li>responding to queries or feedback from you;</li>
                                    <li>maintaining and operating the Website;</li>
                                    <li>managing our administrative and business operations;</li>
                                    <li>engaging third party business partners and data processors to perform certain aspects of the Services;</li>
                                    <li>performing customer profiling, market analysis, and research to improve our product and service offerings to you;</li>
                                    <li>preventing, detecting and investigating crime and analysing and managing commercial risks; and</li>
                                    <li>other purposes which are reasonably related to the above.</li>
                                </ol>
                            </li>
                        </ul>
                    </li>

                    <li>
                        WITHDRAWAL OF CONSENT, ACCESS & CORRECTION
                        <ul>
                            <li>
                                If you wish to withdraw your consent to receive information on new products and campaigns, or any other Services, you may do so by:
                                <ol>
                                    <li>unsubscribing from our Website;</li>
                                    <li>clicking the &ldquo;Unsubscribe" link in the email(s) we send to you;</li>
                                    <li>contacting our Data Protection Officer at the email address below; or</li>
                                    <li>writing to us at the address below.</li>
                                </ol>
                            </li>
                            <li>
                                Please note that if you choose not to provide us with certain Personal Data, or to withdraw your consent to our use, disclosure, transfer and/or processing of your Personal Data, we may not be able to provide you with some or all of the Services.
                            </li>
                            <li>
                                We will ensure that the Personal Data in our possession is accurate and complete to the best of our knowledge. 
                            </li>
                            <li>
                                You have a right to request for access and correction of your Personal Data. If you would like assistance in accessing and/or correcting your Personal Data, please contact our Data Protection Officer at the email address below. We will get back to you within 21 days. 
                            </li>
                        </ul>
                    </li>

                    <li>
                       CHILDREN
                       <ul>
                            <li>
                                This Website is directed toward and designed for use by persons aged 16 or older. We do not intend to collect Personal Data from children under 16 years of age, except on some sites specifically directed to children. 
                            </li>
                            <li>
                                We protect the Personal Data of children along with the necessary parental consent in the same manner as it protects the Personal Data of adults.
                            </li>
                       </ul>
                    </li>

                    <li>
                      THIRD PARTY DISCLOSURE & TRANSFER
                       <ul>
                            <li>
                                We do not disclose or transfer your Personal Data to third parties unless we have clearly asked for and obtained your consent to do so (except where permitted and authorised by law).
                            </li>
                            <li>
                                The Personal Data which you provide to us may be stored, processed, transferred between, and accessed from servers located in the United States and other countries which have laws and regulations that may not guarantee the same level of protection of Personal Data as Malaysia. However, we will take reasonable steps to ensure that your Personal Data is handled in accordance with this Privacy Policy, regardless of where your Personal Data is stored or accessed from.
                            </li>
                            
                                <p style='margin-top:11px'><strong>6.1 Disclosure to affiliated companies in the Shiseido Group</strong></p>
                                <p>The Shiseido Group comprises a number of affiliated companies and legal entities located both within and outside Malaysia. We may disclose, where appropriate and to the extent necessary, your Personal Data to such affiliated companies and legal entities for the purposes of corporate reporting, market research and analysis, customer relationship management and other related legal and business purposes. Please note that we provide our affiliated companies and legal entities with only the Personal Data they need for such business and legal purposes, and we require that they protect such Personal Data in accordance with the applicable laws and regulations and this Privacy Policy, and not use it for any other purpose.</p>
                                <p>　　</p>
                                <p><strong>6.2 Disclosure to third party business partners</strong></p>
                                <p>We rely on third party business partners to perform a variety of services on our behalf. In so doing, Shiseido may let service providers, located both within and outside Malaysia, use your Personal Data for the marketing and promotion of products, services or events that may be of interest to you, for market research and analysis, for customer relationship management, and for the fulfilment of your orders for products and services purchased via the Website. Please note that we provide our third party business partners with only the Personal Data they need to perform their services and we require that they protect such Personal Data in accordance with the applicable laws and regulations and this Privacy Policy, and not use it for any other purpose.　　</p>
                                <p>&nbsp;</p>
                                <p><strong>6.3 Disclosure to third party data processors</strong></p>
                                <p>We may use third party service providers, located both within and outside Malaysia, to help us maintain and operate the Website, or for other reasons related to the operation of the Website and Shiseido&rsquo;s business, and they may receive your Personal Data for these purposes. We only provide them the Personal Data they need to provide these services on our behalf. We require these companies to protect the Personal Data in accordance with the applicable laws and regulations and this Privacy Policy, and to not use the information for any other purpose.</p>
                                <p><strong>6.4 Other disclosure</strong></p>
                                <p>We may use and disclose your Personal Data to perform your instructions and, as relevant, (a) comply with legislative and regulatory requirements; (b) protect or defend rights or properties of customers and employees of Shiseido; and/or (c) take emergency measures for the purpose of securing the safety of customers, Shiseido, or the general public.&nbsp;We may also disclose and transfer our personal data to other service providers and/or third parties (which may be located both within and outside Malaysia) in the context of a merger, acquisition or any other corporate exercise involving Shiseido.</p>
                                
                            
                       </ul>
                    </li>

                    <li>
                        SECURITY & PROTECTION
                        <ul>
                            <li>
                                We maintain strict procedures, standards, and security arrangements to protect Personal Data in our possession or under our control. Upon receipt of your Personal Data, whether through physical or electronic means of collection, we will make the necessary security arrangements to protect such Personal Data as are reasonable and appropriate in the circumstances. Such arrangements may comprise administrative measures, physical measures, technical measures, or a combination of such measures.  
                            </li>
                            <li>
                                When disclosing or transferring your Personal Data over the internet, we take all reasonable care to prevent unauthorised access to your Personal Data. However, no data transmission over the internet can be guaranteed as fully secure and you acknowledge that you submit information over the internet at your own risk.
                            </li>
                        </ul>
                    </li>

                    <li>
                        RETENTION OF PERSONAL DATA
                        <ul>
                            <li>
                                We will not retain your Personal Data for any period of time longer than is necessary to serve the purposes set out in this Privacy Policy and any valid business or legal purposes. After this period of time, we will destroy or anonymise any documents containing your Personal Data in a safe and secure manner.
                            </li>
                        </ul>
                    </li>

                    <li>
                       GOVERNING LAW
                        <ul>
                            <li>
                                This Privacy Policy is governed by Malaysia law. 
                            </li>
                        </ul>
                    </li>

                    <li>
                       CONTACT US

                        <p>
                            If you would like to access or correct any Personal Data which you have provided to us, submit a complaint, or have any queries about your Personal Data, please contact our Data Protection Officer by contacting us at [email]. Alternatively, you may write to us at:
                        </p>
                        <ul>
                            <li>Shiseido Malaysia Sdn.Bhd.</li>
                            <li>Unit 7-03, Level 7, Menara UAC, No.12, Jalan PJU 7/5, Mutiara Damansara, 47800 Petaling Jaya, Selangor Darul Ehsan, Malaysia</li>
                            <li>Tel:60-3-7719-1888</li>
                       </ul>
                        
                        
                        
                    </li>

                </ol>


                <p>
                    Date : 20 July 2018
                </p>
            </div>

            <!-- <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div> -->
        </div>
    </div>
</div>

<div id="tnc_modal" class="modal" tabindex="-1" role="dialog" aria-labelledby="tnc_modal_title">
    <div class="modal-dialog modal-xl  modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="tnc_modal_title">Terms and Conditions</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <ol class='tnc-ol'>
                    <li>
                        This Campaign is organized by Shiseido Malaysia Sdn Bhd.
                    </li>

                    <li>
                        Free sample is while stocks last only.
                    </li>

                    <li>
                        The Campaign will run from 15 February 2019 at 12:00:00am until 31 March 2019 at 11:59:59pm.
                    </li>

                    <li>
                        The Campaign is open to residents of Malaysia and permanent residents aged 18 years old and above with valid NRIC. By entering the campaign you warrant that you meet these requirements.
                    </li>

                    <li>
                        The following group of persons shall not be eligible to participate in the campaign: Employees of the Organizer including its affiliated and related companies and their immediate family members (children, parents, brothers and sisters, including spouses); and/or representatives, employees, servants and/or agents of advertising and/or promotion service providers of Organizer including its affiliated and related companies, and their immediate family members (children, parents, brothers and sisters including spouses).
                    </li>

                    <li>
                        The Organizer reserves the right to suspend, modify, terminate or cancel the campaign at any time. These conditions may be amended from time to time by the Organizer without prior notice. All entries received outside the campaign period shall automatically be disqualified.
                    </li>

                    <li>
                        The Organizer reserves the right at its absolute discretion to substitute any of the samples with that of similar value at any time without prior notice. The values of the sample kit are correct at the time of printing. All samples are given on an “as is” basis.
                    </li>

                    <li>
                        Failure by the Organizer to enforce any of its rights at any time does not constitute a waiver of those rights.
                    </li>

                    <li>
                        This Campaign is in no way sponsored, endorsed or administered by, or associated with Facebook. You are providing your information to Shiseido Malaysia Sdn. Bhd. and not to Facebook. The information you provide will only be used for the fulfillment of this campaign only.
                    </li>
                </ol>
            </div>

           <!--  <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div> -->
        </div>
    </div>
</div>



@endsection
