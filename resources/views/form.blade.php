@extends('home')

@section('form')

<div class="form-content-container">
    <form method="post" action="{{ route('submit') }}">
        <div class="row mobile-row fix-width">
            <div class="form-group @if ( $errors->has('email') ) has-error @endif col-xs-10">
                <input id="email" class="form-control email-input" type="text" name="email" value="{{ old('email') }}" placeholder="YOUR@EMAIL.COM" />
                @if ( $errors->has('email') )
                    <span class="help-block label">{{ $errors->first('email') }}</span>
                @endif
            </div>

            {{ csrf_field() }}
            <div class="form-group submit-margin col-xs-2">
                <button class="btn btn-default custom-submit-btn" type="submit">Submit</button>
            </div>
            <div class="col-md-12 col-xs-10 pptnc">
            By clicking submit you agree to the <a href="#" data-toggle="modal" data-target="#privacy_modal">privacy policy</a> and <a href="#" data-toggle="modal" data-target="#tnc_modal">terms and conditions</a>
            </div> 
        </div>
    </form>
</div>
@endsection

@push('modal')
<div id="privacy_modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="privacy_modal_title">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="privacy_modal_title">Privacy Policy</h4>
            </div>
            <div class="modal-body">
                <p>
                    NOTICE AND CONSENT UNDER THE PERSONAL DATA PROTECTION ACT 2010
                </p>

                <p>
                    The Personal Data Protection Act 2010 (hereinafter referred to as the “PDPA”), which regulates the processing of personal data in commercial transactions, applies to Shiseido Malaysia Sdn Bhd (hereinafter referred to as SML, “our”, “us” or “we”). For the purpose of this written notice, the terms “personal data” and “processing” shall have the meaning prescribed in the PDPA.
                </p>

                <ol>
                    <li>
                        This written notice serves to inform you that your personal data is being processed by or on behalf of SML.
                    </li>

                    <li>
                        The personal data processed by us may include your name, national registration identity card (NRIC) number or passport number, date of birth, gender, marital status, race, nationality, contact number, postal address, email address, preferred counter name or location and sensitive personal data such as product purchase and return history, sample history, gift redemption history, facial/body history, skin/hair concern, lifestyle, occupation, gross income range and education profile  provided by you in the documents as prescribed in paragraph 4 of this notice and all other details relating thereto.
                    </li>

                    <li>
                        We are processing your personal data, including any additional information you may subsequently provide, for the following purposes (“Purposes”):
                        <ol>
                            <li>
                                Processing your application in relation to our customer loyalty membership program and purchase of product;
                            </li>

                            <li>
                                Managing and administering our records in relation to our customer loyalty membership program;
                            </li>

                            <li>
                                Communicating with you about our products and services, in particular about promotional offers, marketing information and event programs;
                            </li>

                            <li>
                                Facilitating or enabling any checks that we conduct or by any third party on you from time to time;
                            </li>

                            <li>
                                Complying with the applicable laws;
                            </li>

                            <li>
                                Assisting any government agencies or bureaus or bodies including for the purposes of police or regulatory investigations;
                            </li>

                            <li>
                                Facilitating your compliance with any laws or regulations applicable to you;
                            </li>

                            <li>
                                Communicating with you and responding to your enquiries;
                            </li>

                            <li>
                                Providing services to you;
                            </li>

                            <li>
                                Conducting our internal activities, internal market surveys and trend analysis; or such other purposes as may be related to the foregoing.
                            </li>
                        </ol>
                    </li>

                    <li>
                        Your personal data is and will be collected from you and/or from the information you have provided in your emails, all application/registration forms, information that you may provide us from time to time, and from third parties such as retailers, service providers, banks and insurance companies.
                    </li>

                    <li>
                        Your personal data may be disclosed to parties such as but not limited to auditors, governmental departments and/or agencies, regulatory and/or statutory bodies, our related corporations and/or members ofShiseido Malaysia Sdn Bhd, Warisan TC Berhad, Shiseido Company Limited (Japan) and their respective subsidiaries, business partners, service providers and any such third party requested or authorized by you for any of the above purposes or any other purpose for which your personal data was to be disclosed at the time of its collection or any other purpose directly related to any of the above purposes.
                    </li>

                    <li>
                        If you fail to supply to us the above personal data, we may not be able to process your personal data for any of the above purposes.
                    </li>

                    <li>
                        Your personal data may be transferred to a place outside Malaysia.
                    </li>

                    <li>
                        You are responsible for ensuring that the personal data you provide us is accurate, complete and not misleading and that such personal data is kept up to date.
                    </li>

                    <li>
                        We may request your assistance to procure the consent of third parties whose personal data is provided by you to us and you agree to use your best endeavors to do so.
                    </li>

                    <li>
                        In the event of any inconsistency between the English version and the Bahasa Malaysia version of this notice, the English version shall prevail over the Bahasa Malaysia version.
                    </li>

                    <li>
                        Privacy Preference and How to Contact Us:

                        <p>
                            You may access and request for correction of your personal data and to contact us with any enquiries or complaints in respect of your personal data as follows:
                        </p>

                        <table class="table">
                            <tr>
                                <th>
                                    Brand
                                </th>
                                <td>
                                    SHISEIDO
                                </td>
                            </tr>

                            <tr>
                                <th>
                                    Designation of the contact person
                                </th>
                                <td>
                                    SHISEIDO Camellia Customer Service
                                </td>
                            </tr>

                            <tr>
                                <th>
                                    Phone number
                                </th>
                                <td>
                                    +603 7719 1836
                                </td>
                            </tr>

                            <tr>
                                <th>
                                    Fax number
                                </th>
                                <td>
                                    +603 7727 8263
                                </td>
                            </tr>

                            <tr>
                                <th>
                                    E-mail address
                                </th>
                                <td>
                                    shiseidocamellia@shiseido.com.my
                                </td>
                            </tr>

                            <tr>
                                <th>
                                    Address
                                </th>
                                <td>
                                    Unit 7-03 Level 7 Menara UAC, No 12 Jalan PJU7/5, Mutiara Damansara, 47800 Petaling Jaya, Selangor Darul Ehsan, Malaysia.
                                </td>
                            </tr>
                        </table>

                        <p>
                            In accordance with the PDPA:
                        </p>

                        <ol>
                            <li>
                                We may charge a prescribed fee for processing your request for access or correction; and
                            </li>

                            <li>
                                We may refuse to comply with your request for access or correction to your personal data and if we refuse to comply with such request, we will inform you of our refusal and reason for our refusal.
                            </li>
                        </ol>
                    </li>

                    <li>
                        Limit the process of your Personal Information

                        <p>
                            If you:
                        </p>

                        <ol>
                            <li>
                                Do not wish to receive products and services, in particular about promotional offers, marketing information and event programs from us or third parties; or
                            </li>

                            <li>
                                Would like to cease processing your Personal Information for as specified purpose or in a specified manner.
                            </li>
                        </ol>
                    </li>
                </ol>

                <p>
                    Please notify us in writing to the address set out in Section 11 above. In the absence of such written notification from you, we will deem it that you consent to the processing, usage and dissemination of your Personal Information in the manner, to the extent and within the scope of this notice. If you do not consent to the processing of your Personal Information as above we may not be able to provide you the products or other goods and services you requested.
                </p>

                <p>
                    Date: 21 March 2014
                </p>

                <hr />

                <p>
                    NOTIS DAN PERSETUJUAN DI BAWAH AKTA PERLINDUNGAN DATA PERIBADI 2010
                </p>

                <p>
                    Akta Perlindungan Data Peribadi 2010 (selepas ini dirujuk sebagai "PDPA") , yang mengawal selia pemprosesan data peribadi dalam transaksi komersial, terpakai bagi Shiseido Malaysia Sdn Bhd (selepas ini dirujuk sebagai "SML" atau "kami"). Bagi tujuan notis bertulis ini, terma "data peribadi" dan "pemprosesan" membawa maksud yang ditetapkan dalam PDPA.
                </p>

                <ol>
                    <li>
                        Notis bertulis ini berfungsi untuk memaklumkan anda bahawa data peribadi anda diproses oleh atau bagi pihak SML.
                    </li>

                    <li>
                        Data peribadi anda yang diproses oleh kami termasuk nama, nombor kad pengenalan (NRIC) atau nombor pasport, tarikh lahir, jantina, status perkahwinan, bangsa, kewarganegaraan, nombor telefon, alamat pos, alamat e-mel, nama kaunter atau lokasi pilihan dan data peribadi yang sensitif seperti pembelian produk dan sejarah pengembalian produk , sejarah pensampelan produk, sejarah penebusan hadiah, sejarah muka/badan, kebimbangan kulit/rambut, gaya hidup, pekerjaan, pendapatan kasar dan profil pendidikan yang diberikan oleh anda dalam dokumen sebagaimana yang ditetapkan dalam perenggan 4 notis ini dan semua butir-butir lain yang berkaitan dengannya.
                    </li>

                    <li>
                        Kami memproses data peribadi anda, termasuk semua maklumat tambahan yang anda peruntukkan kemudiannya, bagi tujuan yang berikut ("Tujuan"):
                        <ol>
                            <li>
                                memproses permohonan anda berhubung dengan program keahlian kami dan pembelian produk;
                            </li>

                            <li>
                                mengurus dan mentadbir rekod kami berhubung dengan program keahlian kami;
                            </li>

                            <li>
                                berkomunikasi dengan anda tentang produk dan perkhidmatan kami, khususnya mengenai tawaran promosi, maklumat pemasaran dan program aktiviti;
                            </li>

                            <li>
                                memudahkan atau membolehkan sebarang penyemakan yang dijalankan oleh kami  atau oleh mana-mana pihak ketiga terhadap anda dari semasa ke semasa;
                            </li>

                            <li>
                                mematuhi undang-undang yang diguna pakai;
                            </li>

                            <li>
                                membantu penyiasatan mana-mana agensi kerajaan atau biro atau badan-badan termasuk bagi tujuan penyiasatan polis atau kawal selia yang diambil;
                            </li>

                            <li>
                                memudahkan pematuhan anda dengan mana-mana undang-undang atau peraturan yang berkaitan dengan anda;
                            </li>

                            <li>
                                berkomunikasi dengan anda dan bermaklum balas kepada pertanyaan anda;
                            </li>

                            <li>
                                memperuntukkan perkhidmatan kepada anda;
                            </li>

                            <li>
                                menjalankan aktiviti-aktiviti dalaman kami, tinjauan pasaran dalaman dan analisis trend, atau bagi tujuan lain sebagaimana yang berkaitan kepada perkara di atas.
                            </li>
                        </ol>
                    </li>

                    <li>
                        Maklumat peribadi anda akan dikumpulkan dari anda dan/atau daripada maklumat yang telah diperuntukkan dalam e-mel anda, semua borang-borang permohonan/pendaftaran, maklumat yang anda memperuntukkan untuk kami dari semasa ke semasa, dan daripada pihak ketiga seperti peruncit, pembekal perkhidmatan, bank-bank dan syarikat-syarikat insurans.
                    </li>

                    <li>
                        Maklumat peribadi anda mungkin didedahkan kepada pihak-pihak seperti tetapi tidak terhad kepada juruaudit, jabatan dan/atau agensi-agensi kerajaan, badan-badan kawal selia dan/atau berkanun, syarikat berkaitan dan/atau anggota Shiseido Malaysia Sdn Bhd, Warisan TC Berhad, Shiseido Company Limited (Jepun) dan anak-anak syarikat masing-masing, rakan-rakan perniagaan, pembekal perkhidmatan dan mana-mana pihak ketiga seperti yang diminta atau dibenarkan oleh anda bagi mana-mana tujuan di atas atau mana-mana tujuan lain yang mana data peribadi anda adalah untuk didedahkan pada masa pengumpulan atau mana-mana tujuan lain yang berkaitan secara langsung kepada mana-mana tujuan yang di atas.
                    </li>

                    <li>
                        Jika anda gagal memperuntukkan data peribadi di atas kepada kami, kami tidak dapat memproses data peribadi anda untuk mana-mana tujuan di atas.
                    </li>

                    <li>
                        Maklumat peribadi anda mungkin dipindahkan ke suatu tempat di luar Malaysia.
                    </li>

                    <li>
                        Anda bertanggungjawab untuk memastikan data peribadi yang anda peruntukkan kepada kami adalah tepat, lengkap dan tidak mengelirukan dan data peribadi itu dikemaskinikan sehingga tarikh semasa. 
                    </li>

                    <li>
                        Kami dibenarkan meminta bantuan anda untuk memperolehi persetujuan pihak ketiga yang memiliki data peribadi yang diperuntukkan oleh anda kepada kami dan anda bersetuju untuk menggunakan usaha terbaik untuk berbuat demikian. 
                    </li>

                    <li>
                        Sekiranya terdapat percanggahan di antara versi Bahasa Inggeris dan versi Bahasa Malaysia notis ini, versi Bahasa Inggeris akan mengatasi versi Bahasa Malaysia. 
                    </li>

                    <li>
                        Keutamaan Privasi dan Kaedah Menghubungi Kami

                        <p>
                            Anda boleh mengakses dan meminta pembetulan maklumat peribadi anda dan menghubungi kami tentang sebarang pertanyaan atau aduan berkenaan dengan data peribadi anda seperti berikut: 
                        </p>

                        <table class="table">
                            <tr>
                                <th>
                                    Jenama
                                </th>
                                <td>
                                    SHISEIDO
                                </td>
                            </tr>

                            <tr>
                                <th>
                                    Gelaran pihak yang dihubungi
                                </th>
                                <td>
                                    SHISEIDO Camellia Customer Service
                                </td>
                            </tr>

                            <tr>
                                <th>
                                    Nombor telefon
                                </th>
                                <td>
                                    +603 7719 1836
                                </td>
                            </tr>

                            <tr>
                                <th>
                                    Nombor faks
                                </th>
                                <td>
                                    +603 7727 8263
                                </td>
                            </tr>

                            <tr>
                                <th>
                                    Alamat E-mel
                                </th>
                                <td>
                                    shiseidocamellia@shiseido.com.my
                                </td>
                            </tr>

                            <tr>
                                <th>
                                    Alamat pos
                                </th>
                                <td>
                                    Unit 7-03 Level 7 Menara UAC, No 12 Jalan PJU7/5, Mutiara Damansara, 47800 Petaling Jaya, Selangor Darul Ehsan, Malaysia.
                                </td>
                            </tr>
                        </table>

                        <p>
                            Selaras dengan PDPA:
                        </p>

                        <ol>
                            <li>
                                Kami berhak mengenakan bayaran yang ditetapkan untuk memproses permintaan anda untuk akses atau pembetulan; dan 
                            </li>

                            <li>
                                Kami berhak menolak permintaan anda untuk akses atau pembetulan data peribadi anda dan jika kami menolak permintaan itu, kami akan memaklumkan anda tentang penolakan dan sebab penolakan kami. 
                            </li>
                        </ol>
                    </li>

                    <li>
                        Mengehadkan Pemprosesan Maklumat Peribadi Anda
                        <p>
                            Sekiranya anda:
                        </p>

                        <ol>
                            <li>
                                tidak ingin menerima produk dan perkhidmatan, khususnya mengenai tawaran promosi, maklumat pemasaran dan program aktiviti dari kami atau pihak ketiga; atau 
                            </li>

                            <li>
                                ingin berhenti memproses Maklumat Peribadi anda untuk tujuan tertentu atau dengan mengikut cara yang ditentukan.
                            </li>
                        </ol>
                    </li>
                </ol>

                <p>
                    Sila maklumkan kepada kami secara bertulis ke alamat yang dinyatakan di dalam Seksyen 11 di atas. Tanpa pemberitahuan bertulis daripada anda, kami akan menganggap bahawa anda bersetuju dengan pemprosesan, penggunaan dan penyebaran maklumat peribadi anda dengan cara yang termaktub dalam skop notis ini. Jika anda tidak bersetuju untuk memproses Maklumat Peribadi anda seperti di atas kami tidak dapat memperuntukkan produk atau barangan lain dan perkhidmatan yang anda minta kepada anda.
                </p>

                <p>
                    Tarikh: 21 March 2014
                </p>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
@endpush

@push('modal')
<div id="tnc_modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="tnc_modal_title">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="tnc_modal_title">Terms and Conditions</h4>
            </div>
            <div class="modal-body">
                <ol>
                    <li>
                        This Campaign is organized by Shiseido Malaysia Sdn Bhd.
                    </li>

                    <li>
                        The deluxe-size gift is while stocks last only.
                    </li>

                    <li>
                        The Campaign will run from 24 November 2018 at 12:00:00am until 23 December 2018 11:59:59pm.
                    </li>

                    <li>
                        The Campaign is open to residents of Malaysia and permanent residents aged 18 years old and above with valid NRIC. By entering the campaign you warrant that you meet these requirements.
                    </li>

                    <li>
                        The following group of persons shall not be eligible to participate in the campaign: Employees of the Organizer including its affiliated and related companies and their immediate family members (children, parents, brothers and sisters, including spouses); and/or representatives, employees, servants and/or agents of advertising and/or promotion service providers of Organizer including its affiliated and related companies, and their immediate family members (children, parents, brothers and sisters including spouses).
                    </li>

                    <li>
                        The Organizer reserves the right to suspend, modify, terminate or cancel the campaign at any time. These conditions may be amended from time to time by the Organizer without prior notice. All entries received outside the campaign period shall automatically be disqualified.
                    </li>

                    <li>
                        The Organizer reserves the right at its absolute discretion to substitute any of the samples with that of similar value at any time without prior notice. The values of the sample kit are correct at the time of printing. All samples are given on an “as is” basis.
                    </li>

                    <li>
                        Failure by the Organizer to enforce any of its rights at any time does not constitute a waiver of those rights.
                    </li>

                    <li>
                        This Campaign is in no way sponsored, endorsed or administered by, or associated with Facebook. You are providing your information to Shiseido Malaysia Sdn. Bhd. and not to Facebook. The information you provide will only be used for the fulfillment of this campaign only.
                    </li>
                </ol>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
@endpush

@push('js')
<script>
$(function () {
    $('input').change(function () {
        $(this).parent('.has-error').removeClass('has-error').find('.help-block').remove();
    });
});
</script>
@endpush
