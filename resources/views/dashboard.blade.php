@extends('layouts.admin')

@section('content')
<div class="row">
    @if ( Auth::user()->username == 'webqlo' )
    <div class="col-xs-12">
        <div class="row">
            <div class="col-xs-10">
                <blockquote>
                    <strong>Start Time: </strong><span id="start-time">{{ Carbon\Carbon::parse( $campaign_time['start'] )->format('j F Y, H:i') }}</span>
                    <br>
                    <strong>End Time: </strong><span id="end-time">{{ Carbon\Carbon::parse( $campaign_time['end'] )->format('j F Y, H:i') }}</span>
                </blockquote>
            </div>
            <button type="button" class="btn btn-primary col-xs-2" data-toggle="modal" data-target="#campaign_date_modal">
                Edit
            </button>
        </div>
    </div>
    <div class="col-xs-12">
        <a class="btn btn-primary" href="/admin/generate-code/5000" target="_blank" style="margin-top: 10px;">Generate 5k Unique Codes</a>
        <a class="btn btn-primary" href="/admin/export-code-list" target="_blank" style="margin-top: 10px;">Export Generated code XLSX</a>
        <br/>
    </div>
    
    <div class="col-xs-12"><br/></div>
    @endif
    <div class="col-md-6">
        <div class="panel panel-default">
            <div class="panel-heading"><strong>Total Registration by Date</strong></div>

            <div class="panel-body">
                <table id="total_registration_table" class="table table-bordered" width="100%">
                    <thead>
                        <tr>
                            <th>Date</th>
                            <th>Total</th>
                        </tr>
                    </thead>
                    <tbody>
                        <!-- Total Registration by DateTables -->
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    @if ( Auth::user()->username == 'webqlo' )
        <div class="col-md-6">
            {{-- <blockquote>
                <strong>End Time: </strong>{{ Carbon\Carbon::parse( env('END_TIME') )->format('j F Y, ga') }}
            </blockquote> --}}

            <div class="panel panel-default">
                <div class="panel-heading"><strong>Total Link Clicks</strong></div>

                <div class="panel-body">
                    <table id="total_registration_table" class="table table-bordered" width="100%">
                        <thead>
                            <tr>
                                <th>Facebook</th>
                                <th>Facebook (from email)</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>{{ DB::table('clicks')->where('target', 'facebook')->count() }}</td>
                                <td>{{ DB::table('clicks')->where('target', 'facebook-email')->count() }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    @endif
</div>

<div class="row">
    <div class="col-xs-12">
        <div class="panel panel-default">
            <div class="panel-heading"><strong>Registration List</strong></div>

            <div class="panel-body">
                <div class="form-group text-right">
                    <a class="btn btn-primary" href="{{ route('export-registration-list') }}" target="_blank">Export XLSX</a>
                </div>

                <table id="registration_list_table" class="table table-bordered" width="100%">
                    <thead>
                        <tr>
                            <th>Email Address</th>
                            <th>Redeem Code</th>
                            {{-- <th>Redeemed</th> --}}
                            @if ( Auth::user()->username == 'webqlo' )
                            <th>Register Time</th>
                            <th>Action</th>
                            @endif
                        </tr>
                    </thead>
                    <tbody>
                        <!-- Registration List by DateTables -->
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

@push('modal')
<div class="modal fade" id="campaign_date_modal" tabindex="-1" role="dialog" aria-labelledby="modal_title" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modal_title">Edit Campaign Date</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Please select the start and end date of the campaign.</p>
                <p>
                    Example:
                    <br>
                    Start date is 1 April 2018 00:00:00am, End date is 30 April 2018 at 11:59:59pm.
                    <br>
                    Just choose <b>1 April 2018</b> and <b>30 April 2018</b> will do.
                </p>
                <div class="input-daterange input-group" id="datepicker">
                    <input id="start_date" type="text" class="input-sm form-control" name="start" />
                    <span class="input-group-addon">to</span>
                    <input id="end_date" type="text" class="input-sm form-control" name="end" />
                </div>
                <button id="save_btn" type="submit" class="btn btn-primary">Save changes</button>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
@endpush

@push('js')
<script>
$(function () {
    $('.input-daterange').datepicker({
        format: 'yyyy-mm-dd',
    });

    $('#save_btn').on('click', function (e) {
        e.preventDefault();
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        var start_date = $('#start_date').val();
        var end_date = $('#end_date').val();
        var start_date_time = start_date + 'T00:00:00+08:00';
        var end_date_time = end_date + 'T23:59:59+08:00';
        var start_html = new Date(start_date_time);
        var end_html = new Date(end_date_time);
        $.ajax({
            type:'POST',
            data: {_token: CSRF_TOKEN, start: start_date_time, end: end_date_time},
            url: '{{ route('save-date') }}',
            dataType: 'JSON',
            success: function(data) {
                alert(data.msg);
                $('#start-time').html(moment(start_date_time).format('DD MMM YYYY, HH:mm'));
                $('#end-time').html(moment(end_date_time).format('DD MMM YYYY, HH:mm'));
            }
        });
    });
    $('#total_registration_table').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{{ route('datatables-total-registration') }}',
        columns: [
            {data: 'date', name: 'date', orderable: false},
            {data: 'total', name: 'total', orderable: false}
        ]
    });

    $('#registration_list_table').DataTable({
        responsive: true,
        processing: true,
        serverSide: true,
        ajax: '{{ route('datatables-registration-list') }}',
        order: [],
        columns: [
            {
                data: 'email'
            },
            {
                data: 'code'
            }
            // {
            //     render: function(data, type, row, meta) {
            //         if (row.redeem == true) {
            //             return ''
            //         }
            //     }
            // }
            @if ( Auth::user()->username == 'webqlo' )
            ,
            {
                data: 'time'
            },
            {
                render: function(data, type, row, meta) {
                    return '<a class="btn btn-default btn-sm" href="/admin/resend-email/' + row.code + '" target="_blank">Resend Email</a>';
                }
            }
            @endif
        ]
    });
});
</script>
@endpush
