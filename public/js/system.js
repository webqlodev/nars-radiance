$(document).ready(function () { 
	$('#entryForm').submit(function(event) {
		event.preventDefault();
		$('.btn-redeem').html("<img class='spinner-img' src='../images/spinner.gif'/>").prop('disabled', true);
		if ($('input#termsCondition').is(':checked')) {
			var bTermsCondition = '1';
		}else{
			var bTermsCondition = '';
		}
		$.post($(this).attr('action'), {
			email: $('#email').val(),
			termsCondition : bTermsCondition,
			_token: $('input[name=_token]').val()

		}).done(function(data) {
			$('#thankyouModal').modal('show');
			$('.btn-redeem').text('Redeem Now').prop('disabled', false);
			$('#entryForm')[0].reset();
			$('#termsCondition').prop('checked', false);

		}).fail(function(jqXHR, textStatus, errorThrown) {
			if(jqXHR.responseJSON.b_success == 0){
				$('#errorModal').modal('show');
			}

			$.each(jqXHR.responseJSON,function(tKey,objError){
				$('#'+tKey+'FormGroup').addClass('error');
				$('#'+tKey+'Error').removeClass('d-none');
				$('#'+tKey+'Error').text(objError[0]);
			});
			// var objError = jqXHR.responseJSON.meta.error;

			// $.each(objError,function(tKey,objError){
			// 	$('#emailFormGroup').addClass('error');
			// 	$('#emailError').removeClass('d-none');
			// 	$('#emailError').text(objError[0]);
			// });
	    	$('.btn-redeem').text('Redeem Now').prop('disabled', false);
	    });
	});

	$('input').on('keyup keypress blur change',function(){
		var tId = $(this).attr('id');
		$('#'+tId+'Error').addClass('d-none');
		$('#'+tId+'Error').html('');
		$('#'+tId+'FormGroup').removeClass('error');
	});

});